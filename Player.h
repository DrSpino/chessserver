#ifndef DEF_PLAYER
#define DEF_PLAYER

#include <string>

class Player
{
    public:

    Player(std::string name, std::string country, int elo);
    
    std::string m_name;
    std::string m_country;
    int m_elo;
};

#endif