#include "Player.h"
#include <string>

using namespace std;

Player::Player(string name, string country, int elo)
{
    m_name = name;
    m_country = country;
    m_elo = elo;
}